#
# MIT License
#
# Copyright (c) 2011-2018 Pedro Henrique Penna <pedrohenriquepenna@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.  THE SOFTWARE IS PROVIDED
# "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
# LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
# ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

ifeq ($(ARCH),k1bdp)
	export BUILD_SERVERS=false
	export BUILDING_CCLUSTER=true
else
	export BUILD_SERVERS=true
	export BUILDING_IOCLUSTER=true
endif

# Toolchain
export CC = $(TOOLCHAIN)/bin/k1-gcc
export LD = $(TOOLCHAIN)/bin/k1-ld
export AR = $(TOOLCHAIN)/bin/k1-ar

# Toochain Location
export TOOLCHAIN=/usr/local/k1tools

# Compiler Options
export CFLAGS += -D_KALRAY_MPPA256
ifeq ($(ARCH),k1bdp)
	export CFLAGS += -march=k1b -mos=nodeos -mcluster=node 
	export CFLAGS += -D_KALRAY_MPPA_CCLUSTER_
else
	export CFLAGS += -march=k1b -mos=rtems -mcluster=ioddr 
	export CFLAGS += -D_KALRAY_MPPA_IOCLUSTER_
endif

# Linker Options
export LDFLAGS += -Wl,--defsym=_LIBNOC_DISABLE_FIFO_FULL_CHECK=0 -O=essai
ifeq ($(ARCH),k1bdp)
	export LDFLAGS += -march=k1b -mos=nodeos -mcluster=node 
else
	export LDFLAGS += -march=k1b -mos=rtems -mcluster=ioddr 
endif

# Binaries & Libraries
ifeq ($(ARCH),k1bdp)
	export LIBMPPAIPC = $(TOOLCHAIN)/k1-nodeos/lib/mOS/libmppaipc.a
	export LIBKERNEL = $(LIBDIR)/libkernel-k1bdp.a
	export LIBUSER = $(LIBDIR)/libuser-k1bdp.a
	export LIBS=$(LIBUSER) $(LIBKERNEL) $(LIBMPPAIPC)
else
	export LIBMPPAIPC = $(TOOLCHAIN)/k1-rtems/lib/libmppaipc.a
	export LIBKERNEL = $(LIBDIR)/libkernel-k1bio.a
	export LIBUSER = $(LIBDIR)/libuser-k1bio.a
	export LIBNANVIX = $(LIBDIR)/libnanvix-k1bio.a
	export LIBSERVERS = $(LIBDIR)/libservers.a
	export LIBS=$(LIBNANVIX) $(LIBSERVERS) $(LIBUSER) $(LIBKERNEL) $(LIBMPPAIPC)
endif
